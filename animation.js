var imgIndex = 1;
let sliceAnimationTime = 1.2;
let imgScaleAnimationTime = 0.5;
let transitionTime = 5000;
let ti;

// start and reset function
function start() {
  let tl = gsap.timeline();
  imgIndex = 1;
  for (let i = 1; i <= 4; i++) {
    for (let j = 1; j <= 8; j++) {
      tl.set(".image-" + i + ">.sect" + j, { y: "0%" });
    }
  }

  //   first call
  sliceAnimation(imgIndex);

  //   for subsequent calls after 9 second internval
  ti = setInterval(() => {
    sliceAnimation(++imgIndex);
  }, transitionTime);
}

// to give the scale down animation
function imgScale(index) {
  let tl = gsap.timeline();
  tl.fromTo(
    ".image-" + index,
    imgScaleAnimationTime,
    {
      scale: 1.5,
      ease: Power3.easeInOut
    },
    { scale: 1 },
    0.5
  );

  //   clearing interval after 4 cycles and calling the start function
  if (index === 4) {
    clearInterval(ti);

    setTimeout(() => {
      start();
    }, (transitionTime-1));
  }
}

// slice transition animation function
function sliceAnimation(index) {
  let tl = gsap.timeline();
  imgScale(index + 1);
  for (let i = 1; i <= 8; i++) {
    if (i === 1 || i === 3 || i === 7) {
      tl.to(
        ".image-" + index + ">.sect" + i,
        sliceAnimationTime,
        {
          y: "-=100%",
          ease: Power3.easeInOut
        },
        0.15
      );
    } else if (i === 2 || i === 5 || i === 8) {
      tl.to(
        ".image-" + index + ">.sect" + i,
        sliceAnimationTime,
        {
          y: "-=100%",
          ease: Power3.easeInOut
        },
        0.3
      );
    } else if (i === 4 || i === 6) {
      tl.to(
        ".image-" + index + ">.sect" + i,
        sliceAnimationTime,
        {
          y: "-=100%",
          ease: Power3.easeInOut
        },
        0.25
      );
    }
  }
}

start();
